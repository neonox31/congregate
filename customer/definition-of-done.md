**DRAFT**

## Definition of Done

This document provides standard definition of done and acceptance criteria for the migration effort.

### Migration PS Project Definition of Done

The customer agrees that the PS project has been completed when these criteria are satisfied.  This does not include any customer-specific actions that need to be taken post migration.

- All groups, projects, and users, up to the number defined in the SOW, and identified by the customer during the discovery phase, have been migrated to the destination instance
- User conflicts identified during the migration have been resolved
- Project conflicts identified during the migration have been resolved
- Questions that have arisen and brought to the migration team during the migration effort have been answered
- Any outstanding migration issues have been addressed, either through resolution or an acceptable workaround that customer has agreed to
- Admin tokens used in source and destination instances have been revoked and admin accounts created for the PSE have been disabled
- The customer understands and has a plan for resolving any manual updates to data that need to happen pre or post-migration
- GitLab has provided the completed Migration Plan document that details the plan and execution of the migration activities

#### Individual Project Migration Acceptance Criteria

The portfolio stakeholders agree that the automated migration of data for a project has been completed when these criteria are satisfied.

- All project data that should be migrated has been (as identified in the migration)
- Any mismatches identified in the migration validator report have been successfully resolved or agreed by customer stakeholders to accept
- User conflicts have been resolved
- If a project conflict occurred, it was resolved as defined in the project resolution document
- User permissions have been validated in the destination instance
- Information has been provided to the project team regarding next steps for making any additional adjustments to restore their project to a fully working state
